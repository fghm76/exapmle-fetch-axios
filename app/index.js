const express = require('express')
const app = express()
const http = require('http')
const bodyParser = require('body-parser')
let users = []
const crypt=require("crypto")
const fileUpload=require('express-fileupload')
let data=[
    {id:0,title:"test1",price:"123212",image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8X4_gR2KPr6Xn4fxCCo5iDmS08ydhzLKdIA&usqp=CAU"},
    {id:1,title:"test2",price:"2000",image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRaZGIkSmdhKebb7pMc8DTVq9vKm6JWsmHk9w&usqp=CAU"},
    {id:2,title:"test3",price:"2100000",image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4AENb3kKLcvGHU8CrxUDJ5rM2pp5EnWCSbw&usqp=CAU"},
    {id:3,title:"test4",price:"320023",image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfvbPJW72Lrhsp4hyiJ6QbsOi2w_MQiBnuRg&usqp=CAU"},
    {id:4,title:"test56",price:"32000",image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiFOzv79ZCxTJoaslVdrHJqQ5-v3mmzvuuSA&usqp=CAU"},
]
let id = data.length    
function App() {

    const server = http.createServer(app)

    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({extended: true}))
    app.use (fileUpload({
        createParentPath: true
        }))
    app.get('/', (req, res) => {
        res.status(200).json({data:data})
    })

    app.post('/add', (req, res) => {
        try {
        var errors = []
        if (req.body.title.length < 1) {
        errors.push({ key:"title", errortext:"عنوان حتما باید وارد شود"})
        res.status(400).json({ errors: errors })
        return
    }
      
        var price = req.body.price && req.body.price !=="" ? req.body.price:'0'
        var image = req.body.image && req.body.image !=="" ? req. body.image:""
        data.push({
        id: id + 1,
        title: req.body.title,
        price: price,
        image: image,
        })
        id++
        res.status(201).json({ data: data })
        }
        catch (e) {
        res.status (500).json({})
        }
        })

        app.delete('/delete', (req, res) => {
            let data2 = data
            data = data2. filter((d) => {
            return !(d.id == req.query.id)
            })
            res.json({data:data})
            // res.json(req.query.id)
        })
        app.put('/update', (req, res) => {
            var errors = []
            if (req.body.title.length < 1) {
            errors.push({ key:'title',  errortext:'عنوان حتما باید وارد شود'})
            res.status(400).json({ errors: errors })
            return
            }
       
            data. forEach(d => {
            if (d.id == req.query.id) {
            d.title = req.body.title
            }
        })
        res.status(200).json({data:data})
    })
    app.get('/paginate', (req, res) => {
//  let [token_type, token] = req.header('Authorization') && req.header('Authorization').length > 0 ?
// req.header('Authorization' ).split(' ') : ["",""]

// let isLogin = false

// if (token =="" || token_type == "") {

// isLogin = false

// } else {
// users.forEach(user => {

// if (user.token == token && user.token_type == token_type) {

// isLogin = true
// }
// })
// }
// if (!isLogin) {

// res.status(401).json('not authorize')

// }
        if (req.query.page == 1) {
        res.status (200).json({
        current_page: req.query.page,
        count: data.length,
        last_page: 2,
        data: [
        {
        id: 1, title:"reactNative", price:"600,000", image:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN4AAADjCAMAAADdXVr2AAAApVBMVEUAfcX////+/v7t7e3s7OwAfMX6+vr39/f09PTx8fEAdcIAd8P7+/sAesQAdMKxzuHt9frA3O/d5eoAgMbW4ejl6uz38+/y+fz6/f7O3ufZ6vU9lc+axeSu0enU5/TK4vIahsimyN+Ju99fotGUwOHj8Pi+1eRtqdNXodREls0ojMt9td1oqtiGt9iy0+uiyuasxd2dy+eQt9h9rdV9ud91rtVhn9Dvb+P0AAAgAElEQVR4nN19C4OiuLYuAYOGELVEUEurRC0txXqcOz01//+n3SRAyEqCYHX1PmcPe3Y3zct8SdZ7ZcXz+YFIwI8REucDfjaQF0fiIhEXkbwY8lM0bC6a92PjvvjoYCguhuJ+IC9q9wPXfdkScdGPy5bAq6i82vy+1ugYNkp+wFPwBha8gQFPNq+52HW/bL74VNi0qf1+A69sSaxaovWUr8EzGx3DRv2H4QVIHNXoIfTvgBdKUCRIZ7OjOB7n/JhNF0uEsoyQIPzD8MqfH/BjJM58ccZnPD9G4ozI+/KibOlQnJUDYd6P1f3yIiFZlh2fHzbXw6XIE0bH9cFwnq//OXxtJquFeDKW7xstiZuWlFeHrY+6Gy2uekQcw5AfQ3E2ivlZPIIXibgYyosjcRaS1vvyYjgaZaPZdvO6xpRGlDKGPew1B/8XZoxRcSSX/W4+5Y2JzZaIs/IiCV1Xjd/XGl22TzzqBQ1T5IM5qJjeADC1QXNRTAZUToaW+6LfTru3AgtcApWChtUf+r85yii5/P1wDPhcrdhv05IgUEx51FwdwEYHRqPLGSwereAZZNPM9bB9rptMUXLvEB2f3nIWsRqZ+BOX5wqpvIzl5eoJRiNc/JrMMoRsArbICqlG+0ODbBsCDX4aHsnI6X3NZ6OEVB3lmbyggKrL1d/lTU6c+PJ5lAT8c/BcXCNQpBy2k7J1//SaiPlYtb8cofq/5o8aTQXQq0a2/AcfxWJzDPksrfhD0HCtLlZn8h/xqDcSx1Aezanz4rD14mgYxo/7YkxLCApINTRqHsqJ6Hna2OpHNVE5LV42aTjUPt+vfeZFcf4zgiHd8TlZzUCsRs/zvGYWapPVU2fVoHqKA5XjyiLv7dkXQ3iPYDCHWAqG3xbrCB2vScQ0gvI0KCaROUbOqzgMeJ+Oi02a/a9rLSO0fWMU19RUNdXzwGSsUNQyr5EK9V1Pf7h6iEXJdV429LfhDVrgOeVMA49kpwunONwwk5qFwGGs2QmuMIDLGr+BA44pe32UksINb9ACb2DA84XAD0byWiBOY3lRnMqLvrxYMmJxSuqXJkU5Ky3ysuCZhOc4XK9R+vbI2yp+MygFkWyKajRxNrpsn7jqGeqiKRjaSTmIt5dIV7X+yMEBvs5JL1bnFAzfFevZ6ixlXN9mwo5gd/QL9b4W6Nti/Tvwgiz9iu4A57H8/1ENa7K5Bx+myZMcvf8QPBTsEnqrPfaF43as/ROnO+p4qP2D0frZNiX7wPsO7T1eohvgaJQbLcVsix60VzCeZe/2J6IbQ8rodUq+QXvf4JzvrLUdnBPkX6s3OG9xNMnIRIfHpj66GvjY/uHtBkIc5VsEOWfgaPTQ4Jz3yr1sVbTySzYurqcs243h5egzC0x4vGfPcH7j6Bmhh4PX+nU8vi7Rd+TeHVpLyIeurXvx23bJu2xlUBHd8/cteD5K1/BLLF8glM24+mrTZfUlPoDBH1TK0LRN1LEo34ghGQZBAVtN34RJ74BHZgaN0lfhdCLZ6tVrAYija0D+nCvpI3EOHTe2L5OS/wyzK5xzrJBGoWNyBuQ0hiiiJ9mSIFxscjdATNcrco/FcIe9N9y7h47R8zYOyyfDE2wXTmZD8X4IOWcqPxp/muzzWP1oONq1kDjzJv4d9l5/az29OGUdiy4n1ZvBtIDo6ImUvWkIhqCcIl/GUF9Q1RI+Bz9yJ0AcvSBno39LKUPPzomJObisEavZK2xv9JKVHwXwPA5PEjhCF/jR8admlqAn5xTF43NKflhrQR8uiYRpMRHuu/olMoHo6FsYOODhGp6PZsYHvSNqWoKWL56rT2kxJ3fB6/JzohfXxKTJZkiAHIK8kBXLWg6hB10pYwre0OgQdkaoaQlC84NrhrLklPXzc7p8w6HppeYqhmlji3l5nqJQ91JngJQwZs9+7dr2weix2ahyPY8yg/zGT77ujx4NT64Zitkua/eSDxsvdS/B4NAxuZI0yQIYY3iGDRlvMsWfAodgqFgBlO5i4hoxhj22Zygef2Ydje7pSkJLF7rxlRAo9mOjofRMGl8vlHt42gTA0NEwBQ/EiBChx7Vjho7fs5/QWtBibdMdzR8srQZtwHMsmQVt8JgGz0eG9KOTIYTH23d12E/0K/sBeIvCQsepboFMeMERWuR0QvrB8w3lGuc2vIxToN0Kju83lbKRCx3dOJS2DDaSXjNdN7hBewgdoRJOX4yW8PaR9GxNUI6PdNFehzkb2ui43g5aXmk1W2AG4TxF4OdMwaA3Z/gEpmd5W48xyEdf7Ak63vu/o7VwrlKjU5+mlwWyI0QmAxyfCORPLXKv8gXDkefcxREhik+WjMelVvRNrcUyOSVBq/AlgAf5A33NDPbborVU8KZQJ+KaqiMARlaFhY8Ln+/D+zIlgtBnkSO+hxaJ3j7BNU14wJVkwmt6R/qy2cUFjxOgJaJwtMs6XUktFsN+bM728a4hZUB7ewofIyapm/DM+3BuR1yVNWmPPxoEBwsf4yPdTnuhirprWQHyYvhp+Ew8hrexyi/QUgliMgNCgZ19M74/MjhnbN5fAcaBc79piZYVEPMJZbrhkmNs5hfENRQPemUqshKpTmhrciqWPOt6t+ZqgnYQxkfxfqDr5S65B/R2Q/eMPpFqCdCWybvVrGJRzmAEG3VTrKNjYs7M/BE54nviFHY9vTa+1j5ivSTgWQLGJC/b7PsmWWUv5vykB3Kv1oL8tcml2N+oDd5BfxYnwXfgZZD3Rhvihhdk70bLcCTVz7u0lleXovm5jBv+0ygIj2DwOC8zdQO3KwnypwAVkPpSZ/CZpO/WtOLsk9iuJNFUz+06ij8NA6+MOkb5JwmHZipB/KZ3BVsPTSePeKOCV6XqHB0/Gk8A16Avoe0vikdOHxr2ZkNnVoPblUTmrY7GZLOo7DulTs6BG4VunTpgl2DgHyXA8YLzJTISP0ja5iAUPqj+Slntim0C5SpxigO8HjPdGMgOrE6nwtKboJtqQKyrxyylTFI9OVEt9wxz5gnEepj+lYuoW5MLUzaofHh/h9by1Rbekt+i7HwK6mAwHzxdocL0uR2eesgNLyBna/hqeNys2Lf5rsvf3Wbt8JAOD21vxbdk86Jik/qlyIICi74h34Q3aHUEmnGaFWSeTxU8TjHPr62Rh+qTReCCR+ysgKUZn3P2liezFkIyA0l+VET69ShaFd83VWrzvi+jbBmUMEWZNJClH0V0G5wntXiVX1CmSfIzh2AgrVPTADhev8wz6ILgpswAmGq9zNnGaDxCETMRnO75mt8KbKpjvHWYs5ZYJ2WUQEuFqv9pAcScCh/UUIun6SoIdFNNE+tjT33GKdZLXeBAtd9il2z6KeNhLheyYiv108USdWotQaBLV/5hlkRY+0k9m0jmDgFZ9RYP2uBFqstuwQPqHcZnKhJnTPFbNSySYYEGIueenfCICHrX3cL/5ryidPVXeVVGR9aDXN4cP4et8MbaG+3wRoeqyZX812dOlRZazilGi0ma10NYPhI9dillaK6GvTQsxYgv38UI6rO0FjqeYQjpqeIG7em2YyvtodEpqnN3dRJRv17/cFQ8jQJFR1U74O+rVHGNc440BUt8NDpJJ/xi445G6Vj5579S0Vwz/yAoOSdWQ8/lnnm/5Jyc2xkxI737NImfbIJMRIvrWGn13fGuXDgQKM4J5V7ZHypFD9N9VjobkP8pVIY6L8xTGamaBiHTFL8+HpdIajWG3BtXHSbT4G25J6K1WTZ/2BcqMxBro6aliXKh+zLIZKOFkY+1bsP56LbWsmaq6WI41nEQVPE9RHYFZc0MUXNeJSyWCCOanP+arNLarFBaS0O6Dl8LWk5PL4ccR1Tllass7PrXSq5EiyeUqUY/U519ClfQDXhkFzVpzvzP8aNfhy9FKjgR6Y0MY+0XcdMKldkuc/fzy6/N9nExzLJM+lo5vCYLWYxetegmywbz09P+n4L3C1MZx1pKZD165ZfH6w+Emqwk5L9rXEE8PkUQnmYxBATqK9zsBqnifPrMr0mn/lDRF6VjluSXw/tmsj0ureBzOn3cPm2+zkXiSWQ9PknpuYxzN/6vMLCaDCwG3UiKN5FK+hWfy1M7Kh+mL1xDUv3biFesjafKO+UoxVDyg2qDh3GRJ9GYX6QCVzPsKt9VUSmuX+OmSr5/jEMzdT18oBXBlDDpUbdHPUGftWAwlE3hzrNzuIIsmLyxft39jTstB6XFboGcOW4HGNt4zSQrq6x1Xax/jjFuZr7wpbblUi8+15GWQd2k0Hpa4rDB9PTha7KTVfptk3cMM3Y9kXyRfK2ysIkb6Wk7aNr8sPiLzVu0lmXNk8sm0VU7POFguRZqHlk6m85q4JA16lWDvpnQSlGo5Y5or3fepRlpTRUvYyuK29Nfma61+MqP+KEc4eJxesiCliVJpZwacp0Iq9TvWjHTiFDJL13HaSgb1yOrQDdaVyNuPLyeautUHSlzaJHj5n3+zkzzczapAuGa6VqCDO+7F7SVXuChyplSw9S01GvGoJ4NJsUBjPr4a5qKuJdMrQV3IfCS+4IjNq/Q92HjpW64xlb5ssRH6WtsqINWDpcWV8BqsJpLzWC2Hc0D2HxQm/DRLuhKFS/0H8f50OVKqh0dFb55xwqwIMw1WSDonzVKRtP3taEBTrF2Uyf3cmqy2u9QffhAOlaAZSVZqf54IpbWgqaR+iFP8FfSBe+ZNX2G6fZ5c8mlTqNToDmkHYeANmbrr8mZNeYX9lL/NrwA5foUYOvMhrenmkzFdB50wCObSA0Bl9N88mTpavNaCFHNai7dUJ2avuBEuyOWKFK83j9MUZxNItyMNZ10wvuMdKIdK5+Biq0vc6wPxmvWtUxDOl3rAWdXqbQFJESL7eat8MZjqUI2k6+eoYCHVNNRKDasWH/tVlVS6GDmaRNdpLHepj2EEv3b7DWraa9qOan6q2oOfby1sllqw0tWkYf4b3wK6vscY4amx4fN6yVnYkTk0mB9olaCu14czIrzr81pnoI+PLOqa8SjRecCN/JONRGLRdoycCWRA9P6mV1Q1wow6dlTbySpJfa50UfS1cPH5vp2XhcFCG95eVIU6/Pbl9C2eUOE9QvWrZezrf48pxTgwnQQywwQ+fgDQa1l1ig2/M/ooRvetdSr5f/ZOXMvyw/KZfmc1HdKWxdvHBfc5hW2UiV9lK9FaVrjmqw9GQDqgpe9MdxQl4gE6PCEodd8DhfLbnhFTVpCmd+RjqIKwpWkFM/Kz+lyJSlNq/68fFwqUDfhCTeDYkYiBIV0VxLnExoPL9OCbq+4mqpvid9fBXbVAdOV5NXP33DjNgR1pQ2vwHnWQXtStGtiiH7qVQfCY+1xK7FLfcy5qr/Ux8xUgAK15R/UL7Xkc8KPavH/0UkPN0Tc0NOrIhhKomif/wl+4JLJR0u55z9FmjEpKEmP6ruqDojAiZJa9BcC91tCKI0N5HAlgaIv/HSmqa04ekJdVQfQVFeYMZ5rYt0/q9Hjj0RPI8Wp2sQ6WTMluXH0gcB9ZwAsqp0ogvTdAbA6kifanBWssRHpF7ot1kulUuOenBsoeMFC1wQxXXYvT5wlmh1Dpz3g6eFLy1PmgPdOFbFitu4Bb6dPZ86NlCspeKDKWcG5/KG76kBwihpNX2hkDouiJfhcjV7LUhfFn8iE6sbwtHtxaaA5mjmaVFUdCDU2xQfvyfTX2FUHhhuqNGbM3mLzvvVStQql5vSplX9gvjQ8Ms34HW/DzlUy4ZnhhmCjrbhWCoZC6cDi7rxH1YFfVNlCmH4SqLQ5BIMMoVT910swoGVRu2aE9flCOqsOoKdIJ5gXVCll6NgQJRbmRPeyfD0HhRtDgXnfIda70nbMYkLoVQs2sNd2X0tNtv5R5y1cr6y0FvRRR2UkY/mrB7wla1QQjBd94I1rJaTUWrrhvVBP6UVsDcp7OeHBTHycTIMKHliTFp1Id9WBR30dc4Eg0wPwKuEJRk+P7ynhqi0lKdusC3bsOeANTHgg6zLaSnixj0ofUtVTeSBiU7EZ1a+jZOWqlIfIU2QvOG15P67v++JUhtaG4kwEpJSBKm2hKb8M72svEXkxnntKERBRbQKrDsSqfXEdcENbkKPxkklXUjCgmhuVvaHuqgMy67D+H30nvQRD7b8ttZYuwcAv5op1YmHDdFcdSBuXI+/1f5B0JUll26vNFbpB3cvyZSJSNXrCXDDut4h1xZvb0naMGX7RzH2uI3cvywdZWzgvtRYiUzdqj4fIKuqERyqXqOyt8aq9ghqM79We5J7wxNKTCp5Qy3rA+0Vrh5KQlZzAObzswDQ/RbLoAU/G7OtXWNoPntaxPZQy/tKTtsiA8/ke8AD/otwm57RH1rq4WEOJ7Ka9GVNOSREm61P1TUzOul970p7QwpWRVugaQwvtBXPAWzay6sAiVy6YOnXJFdXXM7OOY0XB0i/jrOdSccaK3SmtxZNi3bpfvxTUF2My17MVkplVr8WuOgAieEKr9nyZ6FT7IThkK+rvEOtNHkDJavvIvXEje3AfucdnOMWNJ5TNe1TbKdMqKtWMFRmXezBXT6z/6KyVNFLaHf95ukc9CliSSRNv6aeUcSB5wxTweNWjVhL6mzbTCiepgPdJa0IS3umZxRVseNkLVT4v6dboB08xzp7wpMlcjZ43nrSELwG8pxoeLi12r1xAouIb+SCwuIJFylnjBJROQ3cBS8tiaPyyfSwGDu8fVreV9+Im7LAYxO9vdW9udAq80VAvP8IuYas91ZzF2iuYnoZdpen0qgOlhZj2KXUQ67pwmTneUWVuONPTNqIP4UrS9WypknV2bJUYVYm9U9BTMDRd4k4Vt6aAriLTfdgpGPgc1UNSXDJ4Yq15RXtCJbs20/pGMaGi1q7EsbLut2gtDSH101rQhiqV2qO/zGRD17J8ogcyORgvSGtpK/6ONr3g5c3PemItW0+tRcWEesL7iNQ7XIrVBQxuwmvmFX/llcOba5a6F00akXLDz1mvA8EAXpefUxPr3X5OMQ0fNM82Ow+NR11VBwgw8c/IG62YEp3Yi1Z+zI+KtPlR0nMML8ZJ7bHkA5HY97WXhtVFXzOIhJpq3q/fD9XFUew/a4ocW5tN0R+tL2a/aM1s+S+tYw+sY8N4OxOVlY9TfszKGsvimKuLR3G20qJZOLfvz+2XZjvdvsePbS/NtIsz6Oh3Pmq2bw/cCAMPPfRLgQMHbjnv+853fucbrySpgGfcVpZ74yuqmaT6y/yketTT/mvMV+sVpfM0eqj+ljIUjLeaRmif13/beIfD++hYcvLfdQB89F8HDxwcHvmffy08TBccXq8lNf+VBz3+u+E9VvC+w7X/7x//7tETtOdP9LTFf9WB/92CQcJ7+NfC83CplOkWrshxi+TKA3FSnUUyt02dQC1VPlXd1s7k+9VLETWqIFPwfRrVP1n+QPUlWFyY1b+vtSlSLa2val6SUucEUSP8ORHHgzgmxulDc6r7tenGvu946R1YDB+Oj9o/egDG281Hq7Otbu95+cALt3rIlj5LV1KZGzSEp6E6DfUF2NEpNu87XgpBMBJPY+uj9kux3la6912PGm/5e21xKl6PPLjsnFvrfVxJereKqH4fV5KzgOVtV5Lei3TTw5U0sKz1INWtiGjXy9fyS3dhfWV9fS06vD6+Fp0GoqderqQLdPsJT5neR++94On1Htnhj8HTU1zHD33KbhN9XbP0lAUg/vUK4TldSTC/Ca+Jed/pSmqHB1xJuidfF1njx1ZXkr47GvBzviCPEMCfLsiPm6h+XAec4liF6vkZSFrw8oAY9+uAlHbRWXVAfymuo2j8rMpPOII6DMcqKyGu26c9qi6CAF/0hDzoC8aFgyvYVD/Vi0SxRY8YQ2BWeOzeLge+kqd9NsnawhSWMkKkfYWlPSJEZKFrAuNjrwhRWwHL1ggR2YB+J30CYB9AiK9kfE8fil7xPbLUKTj6uBterwAYZPKXrA88MBPL+B6Y4nTXBx5YXC6S0/4EvOyi87yvXqOnS0osorOi/p0HPtNNewHSqyNx8XIv7fWJ7/lBDro96EF7YFaxcyZTxcG1teiOJoDvDtUTXfDhPJP3iWsDAa3qgPYGbqs60OQfBOSoofPoFpVXy58yHq1/H2hgfFbVeS3NkSxb81qaUD2cal7aR+71qbYD5B6o+UOP+qOBW+458lr4d8BQiHI53XuhrEA3nfpoLTcLWDq0lpHO0b2kz14o2TuQC8cSHqh8TT/7wAs845WfV8pGv6C60ScrCXCWxJdZSUGqm6dGRmBLqnhmaHL3WQxeH4sBKIuimEBnRmCQeoCzoLLqQAzq6xR3R/Xx2pnwDV4aGmI97MpPCEHBVbqzq8hZ74cwn3M/rKoOgHqAdBX0SBV/AqqOGIyuxA8jG7dzimzNRnVvUAcK10cPQZUq/gS+9EK6xTp61FWdaEKCn1bKQFuxiHDfl0vtNbnUR1CL69InE36R6z3yfie8HloLzPtek254/tSDAnxQVh2Awt7zZoEsINe+JElwQp1JyaXG9p6qev5BZ3VVI2UOLfUylbI+elvVgRoe9NjK/XPKqgOwJmB00nYtaKs64APlFYsFazd3PRhNwPPClWSuUgMvxdC0mYxuVB2oPuWD3Z2ih7iuOhAAO4KLhu6dSwMoLB+6BcN9WUkESOjxtIvVid+HleVTVXUgmILSfNjvsUEdqGUsF6D9qNYCUoRxgSAvcIp1yJvPGVi/B8a1B7wMCsu7lDKvG14KJsdXH3hAdY42RIMH9Dtt9eUNeHD2HNGPjh4Uk+Xyx9vw0BQsHWcrrRRUDERDSRpdS3GBO0mkZf+kGxdYMaV52EV7T7okFu47reoAUPA8534LVtUBIGUu5EddSctcb2t31QH+AxcTgVZ1ABhFHLsxGZzOBkCv3jzoL9a9Tq3F0B6NurCutbwrwB1pvW69hBfMQUJPtO1clg89bMJH44Ynu5yQDI6eqOkgKymEyAkPrEmjW9IJD5ZtN6sOBBmoPChud8J7ZNCxYSb6C1GbxYvj4/PH7uUK+Jr3tt9vdpPVal6mTQcQ3tAHalS+6IaXglrcoigyKGBJwLphMbidYnSYQ1VO0V5ZEmOx2v31dilyj8n4orGVoAg4jiOc5OvzdfOwSkmWNQQ1fAZD8dq9pXUGd7lKpkivOjAM4wVoLFfyiCyo1FZ1QFx8gVb+sFx6h/zj8+f1kogicu2bZNb9WBZrGY/z8/VplWaZzMwEgT2xjF4WKLCqDsT1RRL7oPVi/QnRqg6IWpBXUza0h1BKERU+g7Xa64xkQXp6eisSa7C6D1lrJyleRflLNNRFmDCGzL0IDBU/MPIfxqegerQpJgS8ucLG6SgmxCcLoBC6nfy6eJQyOyHyHpA0P3xuTAOtoxRUACKBwjMZBFYBS2AMil0PO+HBfSBxdPeYOQ8OUf8n58ld8AjM7ij3OjALWIJYg0evXXXKhugZwvsJbPYRpYG1FwFkdQGBe0dwO71+VN/7ssB6E5PU5bAB/pw4/0OQtIOd484CBTtIV9cylVzufdkMTPgJJ8VX1tJbA1UY9fVHZuNteNeFvRcBnE2Gs4HN3VXFgf+Ek/m8vcKjUIdX70UX2/8JfDQ5PATZDV+LvcNTS9ltKBu1rapsV1JVzPw/cjAqynO2BcBghEvWbbfglSINPij28XLKGUK2B3zXduu/fVC6/hwh5Fp9aWweIcpG6fC0qL8xfLgItKg+qUL12ewlvwcbltIMMntPpp7J8my9P0O96yOq9trUGm2wby9a1feVK0kViBvA4eNWEyTlIBg+v+Gek1LqW1zK5+t/fu03fwOlaDfZvOyvXCVNRKm5njApvUxMVhcGxo6Ub6h9LxRDM8XsCOZ6NpisjWFoOUoV67D/OK3SQTBClqeMlHt8Lcn8NPnrdS3qbfcoJixqbgtbqhHrohgzbHE9g11VxQcGjz2jBl4WvBTWnmdOZOPk/D6ZSw/CMOjwtYh5P0TL6fPmUIy798zAUf6y1MpuH42FGVfFf1zwoF0k9JuyJjwXBNObW62on08u+4cFN2/KiHVfV5Kc/CQ4vVw6fwJT9jLL6kYbBfJlUXFrgzrNTQJdDHJtpaC9ab9q4uytrq34LVeSEQhvBeh9zTJJexuDr+jrhyXtiT7UA/hwt7IylQDNv7x++fI4SfVQvl51AMYYrPvyJZMNtgNMXhZDw8EiGqunEogEBiD3xLm5STH9Gy32rPdiACp2G+uqttNadQCBMIF3yxrmAAcDY7PIaKsZhS17oaS50VFfN2mOGTfFZrqdnrKWABiMxHEJze3H9slKc2NnTlGFtXODOtgQ0dXt2ESN/Y8PqIm/Z254QO61wAN7pWE2y+ayOLvrlyV+eCmZ2vBsU44cek5FSov3GTK3rUzSwBl8NuC53LiQ8kR93oEozt5TSRLlD6x9iBwFAqbWBnf2gTl1fz3LpbnDiWEH+7eqDpQvO6sODOEWYOxR5hcMQ7I791Df6SGGH5X2XmWtS7knTTl+9mHu6WkdXH3YTauOhzsX8WNaCwYp12Rv2nLPuC/2QjkZlFdPAVE47DYD8Mr8PzkbZAi3/qp7B7eO6Unp4ZQR5WAmprWMXBGirrQdBD3J5W5i9Q5u6PZeOp6Ymjd2cDPgpTe8DDhK9rOMAK4AGXR0DBzwugJgaGIOnt/AE6M73BXtI8j79Mb+e8plWPnPT1HLZ3BUfC5MkUWgm7GKD4L8g1vwqskJpzhd6fBk+0jANXo3QLEblLZ760D5Oc2sgDIqP9w78bGo+Bj6sRX1j2HTotPIjO/HBryRtesAdCmww7C531TMHU0K1xTF9NkPzV0NxFdbdy61NrPmHxmvP2I+K62dS1EAqU8kuFnxPZDGZAmGIIWmZvQI91uvH/XJxN4iXWj+LfutW2K9JBu0sHfHXm/b9p0dGMyTPpliuyttx9AFuVXaRLB9QFbE9nhD/G4AAAbRSURBVM9xCVnHrXtuq+ujR6tSwHraCs9QdXCyRHdpLZZyfGyFl01MD5yobnkvPM7JTNuVFcfW0YObqsoERcV/euSUZWvzdb8FXrazZpXMHrtz12BOEe8me2HJ0blrMDfVCJTJIlXixq7BplJmSE6pPcZO2kMbS+XAj8hhX1a0Z5izYL/1N5OIMds2fWBE/eHDfML0T1cdweCiF70TkH/Q7Ldu79eN6eTWfutOsV5NBpt9YvaA6hnciHW5w9sU/nC1VK6XWM+MUFMOY0HavHu1xi56AXsd9NJaanjLtYVPlMN3wYMrQMoJ1hMeMTwEcqmIAx5JL5ZMkJt1fxMeHxJLPHCevUROeIYdLDfY7Td6hnuu3PTPgheuHI15JcFNeI1H1KY9fkwT+5PrI3HQHsp2hl8H2F/tGYEZZGFcA9GWkWq0N/EcXa2xOiftObMCmr0Ehsfc+ijznlCsv1SG8kPf8MolWnzfzues4//+ozE1v7Kh1pKqfSPyZVtp9DwaGo3WdrmSu0g5swJ0bdeBj8ulagdbWIPr2WjoIeuRjWsY+yJTQK8GVqXIrNa2EkzPBGTbGfkFHWK9Iht0tKc8putHZKf1mMF2zrM7lTL0Ats93mVmWiE3BXf2xOS9Bxt9p9aiuIKDv3iMbbSqbzW8JXRj4DwNO+DFj6ZfNbOzJhcHh5kwFlylE1671tIoAAtLPvAjWq+ywMwYNApQ0Fe/Q2sJ14ayOQ9UAcu6JR+J6+f3w2BgFbDUtRa5e6J7QQcM1Ycjh3nEB/C9erB5KTYejD5i1yoUllYvhXvj+X1ornc5nh1Dx8VvfHODhOoU7FzqEAw14/9y/kbxUD6qpkAwAwlFfHoKEWAKBrGWwLGUV8zmpWpJNS/eXX5WzHaZs+rAXUpZI7aRyQFqgOd5BmLdITS6PXYm7WIdGbqmR0++3hLeKU7jnCWnzL0s/z6tRdcpH5zuakZfOUA9lH8x/P4vWaunzNTZucgDVQe2a6dzjK7n9aqXnvBQCzw9ZW6VO92DFAtXdSM8j0Yv0BVpGT2Y/iwZbaDgkdHj2emdxuO3cisaV9UBC17rAn5SVh3QQvUDF38WALy9UJ/r/ANjeuIidVYdIP6jESMQi8pFVUrRkux0dkdPMNsgn2ipDK2NFld7CQbF+DfuiBSm7HVV2nfDamM+Hf2b70wVT83w1d91S0L04B45ERY6ZTeqDvR1JZnOhnKuo2fnBOWYKbs8LFG5cfTc6AP6mbnE+iv8FCvKRckkm22KtrhXdJiRoH3d+ne0Fl/PSlocWrIHMBsXfI4KVSYzAvQeW1nRWYSejDksdkrg6ld2evPavO2MPpW6zh+DN8wm9gDiuoXjy07457M3U5ylFrxHwwCgf4kJdXzPx60h2eg8BQvc+sC7i/bE5CPpod3Vz2hy3s0zI0eP6/ZGov/UlHjsnKHHza2sGeZ9os6KHxbtSWu3WsAvgEsVXV6sovrikBfFvmcyvwA95O0AMaP48vJiSofPLYCXGsob9j73xa0MbBxxBYLU7Qv6NFpc7S3W9cLMKL3eyivGzBoEzP4G61pfbR8OvZF4hWn+ARZ29pZ7d8Br5nqQrVyK7o3DUHnuSifEDO+XfZbl/xS8gUhUcaqDf+Bg9CB2N/0evA5XUnuqeBB+3CBB2esNT/02Nsyiy4l0r6Nvsxg67L1bexEMRy8lwD83ioxeHuLwnkbB09qVJAfG95Ej6i/kXtlbvpZfICdLlnZEvH8fnJgs9cD4vhIMjkbHRqPju5Uy34oQDdFHMf4zecc0Opwy5CKrP6i1WAEwbpYd+ibo9j9YlHxxQeeI792ptUCR5ivv4u2qA9p9DnD+nv/MApvy4ELw8rRQ+Qe+lRXgdM46/ZxGgQBtc9sbVQfM+6MhmfxYcjyN8utzbbSFdgGDtvaFRvtClSr+HcFg3U93Fxr1SIi+PW7j5Gs7zAJ3fO9+wfBNse6IEHETO90d8l45386DUVZctwgE8KX22L3A7ae1Fhc88Xq43O7X7N4FKmJNQJScN6tAyoH/G/Dc9/mxmFzXCe03jBIZy8+b5zQjxLmR2G/Buz2N76A9vUAcn6enp+s5l6tNGLb0GqnryPXBrPjnfXKUBNVakeg3aO9uc3bgzIxwtIxkJJ1tPzbXc5EkfJCiaCxKK4/Hcmltkq8PvzaT54UvFna39qGZGXGvOfvbYr31vpxXYTma6Xy1et4+P29P4lg9HqcLJBY7kJZl+b7fltfyn9ZabsIDudJyMsvVVuW8M+//V8Mr23QL/h+FF1jwAqP5UilrLpr3Y+O+nJymJ9+4b5eCqlti5ZQFllJmNDqGjZLw/j/14XKAQUkkRAAAAABJRU5ErkJggg==" },
        { id: 2, title: "react", price: "200,000", image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqeYKcyuiKVWr_8rDMB-EbYX5SfqdxWhssrw&usqp=CAU"},
        { id: 3, title:"flutter", price:"400,000", image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRWPJaRYrywd4eOMjPiFEOCiLX-cPDlljFR6w&usqp=CAU"}
        ]
        })
        return
        } else if (req.query.page == 2) {
        res.status(200).json({
        current_page: req.query.page,
        count: data.length,
        last_page: 2,
        data: [
            {
            id: 4, title:"unity", price:"600,000", image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9W-MkONZ4ywSmgBDRysYntDOyWn06AcSGxA&usqp=CAU" },
            { id: 5, title: "rnode jseact", price: "200,000", image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFDBj9x12b8kAptL3iU2RsBrGjCdBtRj0y9uHaovU9ASi09c_3F_SWM-cKKDXTc9xz4ts&usqp=CAU"},
             ]
            })
        return
        }
        else {
res.status(200).json({
current_page: req.query.page,
count: data.length,
last_page: 2,
data: []

})
return

    }
    } )
    app.post ('/upload', (req, res) => {
        try
        {
        if (req.files.image.mimetype ==='image/png' ||req.files.image.mimetype === "image/jpg"|| req.files.image.mimetype ==='image/svg') {
      
        req.files.image.mv('./upload/' + req.files.image.name)
  
        res.status(201).json([])
     
        return
 
        } else {
    
    
        res.status(401).json({ errorText:'فرمت فایل وارد شده اشتباه است'})
        }
      
        } catch (e) {
     
        res.status(500).json({})
        
        }
        
        })
        app.get('/profile', (req, res) => {
            let [token_type, token] = req.header('Authorization').split(' ')
            users. forEach(user => {
            if (user.token == token && user.token_type == token_type) {
            res.status(200).json({ user: user })
            return
            }
            })
            res.status(401).json("not auth")
            res. json({ token_type, token })
        })
    app.post('/signup', (req, res) => {
        try{
        var errors = []
            var pattern = /^[a-z0-9]{1,}@[a-z0-9]{1,}\.[a-z]{1,}$/i
            if (!pattern.test(req.body.email) || req.body.password.length < 6) {
                if (!pattern.test (req.body.email)) {
            errors.push({ key:  'email', errortext:'فرمت ایمیل اشتباه وارد شده است'}
            )
                }
            if (req.body.password.length < 6) {
            errors.push({ key: "password",   errortext:'رمز باید بیشنر از ۶ کاراکتر باشد' })
        }


         res.status (400).json({ errors: errors })
         return
    }
    var err = false
    users. forEach (user =>
    {
    if (user. email === req.body.email ) {
    success = true
    }
    
    })
    if (err) {
        errors.push({key:'email',errortext:'با این ایمیل قبلا ثبت نام انجام شده است'})
    res.status(400).json({errors: errorText})
    return
    }
    users.push({email:req.body.email,password:req.body.password,token:"",token_type:""})
    console.log(users)
    res.status(201).json([])
    } catch (e) {
    res.status(500).json({})
    }
            })
    app.post('/signin', (req, res) => {
        try{
        var errors = []
            var pattern = /^[a-z0-9]{1,}@[a-z0-9]{1,}\.[a-z]{1,}$/i
            if (!pattern.test(req.body.email) || req.body.password.length < 6) {
                if (!pattern.test (req.body.email)) {
            errors.push({ key:  'email', errortext:'فرمت ایمیل اشتباه وارد شده است'}
            )
                }
            if (req.body.password.length < 6) {
            errors.push({ key: "password",   errortext:'رمز باید بیشنر از ۶ کاراکتر باشد' })
        }


            res.status (400).json({ errors: errors })
            return
    }
    var token = crypt.createHmac('sha256',Date.now + req.body.email).digest("base64")

    var success = false
    users. forEach (user =>
    {
    if (user. email === req.body.email && user.password === req.body.password) {
   
        user.token = token
        user.token_type="Bearer"
        success = true
    }
    
    })
    if (! success) {
    res.status(403).json({ errorText:'کاربری با  این مشخصات یافت نشد'})
    return
    }
    res.status(200).json( {token: token, token_type:"Bearer"})
    } catch (e) {
    res.status(500).json({})
    }
            })
            
        server.listen(9000, () => {
            console.log('server runnig on port 9000')

    })
}

module.exports = App